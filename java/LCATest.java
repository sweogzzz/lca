import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class LCATest {
	
	@Test
	public void testDAG() {
		assertThrows(IllegalArgumentException.class, () -> {
			new DAGLCA(-1);
			new DAGLCA(-10);
			new DAGLCA(-100);
		});
		assertEquals("Constructor size of 0: ", 0, new DAGLCA(0).v);
		assertEquals("Constructor size of 1: ", 1, new DAGLCA(1).v);
	}

	@Test
	public void testHasVDAG() {
		assertFalse("Size of 0, index 0: ", new DAGLCA(0).hasV(0));
		assertFalse("Size of 0, index 1: ", new DAGLCA(0).hasV(1));
		assertFalse("Size of 0, index 2: ", new DAGLCA(0).hasV(2));
		assertFalse("Size of 0, index 3: ", new DAGLCA(0).hasV(3));
		assertFalse("Size of 0, index -1: ", new DAGLCA(0).hasV(-1));
		assertTrue("Size of 1, index 0: ", new DAGLCA(1).hasV(0));
		assertTrue("Size of 2, index 0: ", new DAGLCA(2).hasV(0));
		assertTrue("Size of 2, index 1: ", new DAGLCA(2).hasV(1));
	}

	@Test
	public void testAddEDAG() {
		DAGLCA dag = new DAGLCA(5);
		assertTrue("Add edge success 1-2 of 5: ", dag.addE(1, 2));
		assertEquals("Add edge (exiting 1) 1-2 of 5: ", 1, dag.od[1]);
		assertEquals("Add edge (entering 1) 1-2 of 5: ", 0, dag.id[1]);
		assertEquals("Add edge (exiting 2) 1-2 of 5: ", 0, dag.od[2]);
		assertEquals("Add edge (entering 2) 1-2 of 5: ", 1, dag.id[2]);
		assertEquals("Add edge (matrix check) 1-2 of 5: ", 1, dag.a[1][2]);
		assertEquals("Add edge (matrix check) 1-2 of 5: ", 0, dag.a[2][1]);
		assertTrue("Add edge success 1-1 of 5: ", dag.addE(1, 1));
		assertFalse("Add edge success 1-(-1) of 5: ", dag.addE(1, -1));
	}

	@Test
	public void testDelEDAG() {
		DAGLCA dag = new DAGLCA(5);
		dag.addE(1, 2);
		dag.addE(4, 4);
		assertTrue("Del edge success 1-2 of 5: ", dag.delE(1, 2));
		assertEquals("Del edge (exiting 1) 1-2 of 5: ", 0, dag.od[1]);
		assertEquals("Del edge (entering 1) 1-2 of 5: ", 0, dag.id[1]);
		assertEquals("Del edge (exiting 2) 1-2 of 5: ", 0, dag.od[2]);
		assertEquals("Del edge (entering 2) 1-2 of 5: ", 0, dag.id[2]);
		assertEquals("Del edge (matrix check) 1-2 of 5: ", 0, dag.a[1][2]);
		assertEquals("Del edge (matrix check) 1-2 of 5: ", 0, dag.a[2][1]);
		assertFalse("Del edge success 1-3 of 5: ", dag.delE(1, 2));
		assertTrue("Del edge success 4-4 of 5: ", dag.delE(4, 4));
		assertFalse("Del edge success (-1)-(-1) of 5: ", dag.delE(-1, -1));
	}

	@Test
	public void testAcyclicDAG() {
		DAGLCA dag = new DAGLCA(3);
		DAGLCA dagnot = new DAGLCA(3);
		DAGLCA dsmall = new DAGLCA(1);

		dag.addE(0, 1);
		dag.addE(1, 2);
		dag.addE(0, 2);

		dagnot.addE(0, 1);
		dagnot.addE(1, 2);
		dagnot.addE(2, 0);

		dsmall.addE(0, 0);

		assertTrue("Acyclic test 0-1-2 0-2 graph: ", dag.isAcyclic());
		assertFalse("Acyclic test 0-1-2-0 graph: ", dagnot.isAcyclic());
		assertFalse("Acyclic test 0-0 graph: ", dsmall.isAcyclic());

		dsmall.delE(0, 0);

		assertTrue("Acyclic test empty: ", dsmall.isAcyclic());
	}

	@Test
	public void testLCADAG() {
		DAGLCA dag = new DAGLCA(6);
		DAGLCA dagnot = new DAGLCA(7);
		DAGLCA dsmall = new DAGLCA(3);

		dag.addE(0, 1);
		dag.addE(0, 2);
		dag.addE(1, 3);
		dag.addE(2, 4);
		dag.addE(3, 5);
		dag.addE(4, 5);

		dagnot.addE(0, 1);
		dagnot.addE(0, 2);
		dagnot.addE(1, 3);
		dagnot.addE(2, 4);
		dagnot.addE(3, 5);
		dagnot.addE(4, 6);
		dagnot.addE(5, 0);

		dsmall.addE(0, 1);

		assertEquals("LCA DAG input 1&(-1): ", -1, dag.findLCA(1, -1));
		assertEquals("LCA DAG input 1&8: ", -1, dag.findLCA(1, 8));
		assertEquals("LCA DAG applied to cycle: ", -1, dagnot.findLCA(1, 2));
		assertEquals("LCA DAG applied for same inputs: ", 1, dsmall.findLCA(1, 1));
		
		dsmall.addE(2, 2);
		
		assertEquals("LCA DAG applied for small: ", -1, dsmall.findLCA(2, 2));
		assertEquals("LCA DAG applied for 3&4: ", 0, dag.findLCA(3, 4));
		assertEquals("LCA DAG applied for 1&5: ", 1, dag.findLCA(1, 5));
		assertEquals("LCA DAG applied for 4&5: ", 4, dag.findLCA(4, 5));
		assertEquals("LCA DAG applied for 5&3: ", 3, dag.findLCA(5, 3));
		assertEquals("LCA DAG applied for 1&0: ", 0, dag.findLCA(1, 0));
	}

	@Test
	public void testFindPathDAG() {
		DAGLCA dag = new DAGLCA(3);
		dag.addE(0, 1);
		dag.addE(1, 2);

		assertTrue("Path from 0-2: ", dag.findPath(0, 2));
		assertTrue("Path from 0-1: ", dag.findPath(0, 1));
		assertFalse("Path from 0-(-1): ", dag.findPath(0, -1));
		assertFalse("Path from 2-0: ", dag.findPath(2, 0));
	}

	@Test
	public void testNodeTree() {
		Node n = new Node(0);
		assertEquals("Constructor test: ", 0, n.d);
	}

	@Test
	public void testEmptyTree() {
		LCA lca = new LCA();
		assertEquals("LCA for empty tree: ", null, lca.findLCA(1, 2));
	}

	@Test
	public void testSingletonTree() {
		LCA lca = new LCA();
		Node n = new Node(1);
		lca.root = n;
		assertEquals("LCA for singleton: ", n, lca.findLCA(1, 1));
		assertEquals("LCA for singleton: ", null, lca.findLCA(1, 2));
		assertEquals("LCA for singleton: ", null, lca.findLCA(100, 2));
	}

	@Test
	public void testTree() {
		LCA lca = new LCA();
		Node n1 = new Node(5);
		Node n2 = new Node(20);
		Node n3 = new Node(13);
		lca.root = n1;
		lca.root.l = n2;
		lca.root.l.r = n3;
		assertEquals("Common tree: ", true, lca.inTree(n1.d));
		assertEquals("Common tree: ", true, lca.inTree(n2.d));
		assertEquals("Common tree: ", true, lca.inTree(n3.d));
		assertEquals("Common tree: ", false, lca.inTree(50));
		assertEquals("Common tree: ", false, lca.inTree(100));
	}

	@Test
	public void testDoubleTree() {
		LCA lca = new LCA();
		Node n1 = new Node(5);
		Node n2 = new Node(10);
		lca.root = n1;
		lca.root.l = n2;
		assertEquals("LCA for pair: ", n1, lca.findLCA(5, 10));
		assertEquals("LCA for pair: ", n1, lca.findLCA(10, 5));
		assertEquals("LCA for pair: ", null, lca.findLCA(7, 9));
		assertEquals("LCA for pair: ", null, lca.findLCA(100, 60));
	}

	@Test
	public void testLCATree() {
		LCA lca = new LCA();
		Node n1 = new Node(1);
		Node n2 = new Node(2);
		Node n3 = new Node(3);
		Node n4 = new Node(4);
		Node n5 = new Node(5);
		Node n6 = new Node(6);
		Node n7 = new Node(7);
		lca.root = n1;
		lca.root.l = n2;
		lca.root.r = n3;
		lca.root.l.l = n4;
		lca.root.l.r = n5;
		lca.root.r.l = n6;
		lca.root.r.r = n7;
		assertEquals("LCA of numbers 1 + 2: ", n1, lca.findLCA(1, 2));
		assertEquals("LCA of numbers 1 + 2: ", n1, lca.findLCA(2, 1));
		assertEquals("LCA of numbers 3 + 5: ", n1, lca.findLCA(3, 5));
		assertEquals("LCA of numbers 3 + 5: ", n1, lca.findLCA(5, 3));
		assertEquals("LCA of numbers 7 + 4: ", n1, lca.findLCA(7, 4));
		assertEquals("LCA of numbers 7 + 4: ", n1, lca.findLCA(4, 7));
		assertEquals("LCA of numbers 6 + 7: ", n3, lca.findLCA(6, 7));
		assertEquals("LCA of numbers 6 + 7: ", n3, lca.findLCA(7, 6));
		assertEquals("LCA of dummy: ", null, lca.findLCA(8, 9));
		assertEquals("LCA of dummy: ", null, lca.findLCA(10, 50));
		assertEquals("LCA of dummy: ", null, lca.findLCA(3, 400));

	}
}