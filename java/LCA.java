import java.util.ArrayList;
import java.util.List;

class Node {
	int d;
	Node l, r;

	Node(int v) {
		d = v;
		l = null;
		r = null;
	}
}

class LCA {
	Node root;

	Node findLCA(int x, int y) {
		if (!inTree(x) || !inTree(y))
			return null;
		return findLCA(root, x, y);
	}

	Node findLCA(Node node, int x, int y) {
		if (node == null)
			return node;
		if (node.d == x || node.d == y)
			return node;
		Node l = findLCA(node.l, x, y);
		Node r = findLCA(node.r, x, y);
		if (l != null && r != null)
			return node;
		if (l != null)
			return l;
		else if (r != null)
			return r;
		else
			return null;
	}

	boolean inTree(int v) {
		return inTree(v, root);
	}

	boolean inTree(int v, Node node) {
		if (node == null)
			return false;
		if (node.d == v)
			return true;
		boolean l = inTree(v, node.l);
		boolean r = inTree(v, node.r);
		return l || r;
	}
}

class DAGLCA {
	int v, e;
	int[][] a;
	int[] od;
	int[] id;

	DAGLCA(int s) {
		if (s < 0)
			throw new IllegalArgumentException("Invalid size");
		v = s;
		e = 0;
		id = new int[v];
		od = new int[v];
		a = new int[v][v];
		for (int i = 0; i < v; i++)
			for (int j = 0; j < v; j++)
				a[i][j] = 0;
	}

	boolean hasV(int v) {
		return !(v < 0 || v >= this.v);
	}

	boolean addE(int p, int q) {
		if (hasV(p) && hasV(q)) {
			a[p][q] = 1;
			id[q]++;
			od[p]++;
			e++;
			return true;
		}
		return false;
	}

	boolean delE(int p, int q) {
		if (hasV(p) && hasV(q) && a[p][q] == 1) {
			a[p][q] = 0;
			id[q]--;
			od[p]--;
			e--;
			return true;
		}
		return false;
	}

	boolean isAcyclic() {
		int c = 0;
		int[] vd = new int[v];
		for (int i = 0; i < v; i++)
			vd[i] = 0;
		for (int i = 0; i < v; i++, c++) {
			vd[c] = i;
			for (int j = 0; j < v; j++)
				for (int k = 0; k < v; k++)
					if (vd[k] == j && a[i][j] == 1)
						return false;
		}
		return true;
	}

	int findLCA(int p, int q) {
		if (hasV(p) && hasV(q))
			if (e > 0 && isAcyclic())
				return calcLCA(p, q);
		return -1;
	}

	boolean findPath(int a, int b) {
		List<Integer> v = new ArrayList<Integer>();
		List<Integer> q = new ArrayList<Integer>();
		q.add(a);
		do {
			int c = q.get(0);
			q.remove(0);
			if (c == b)
				return true;
			if (!v.contains(c)) {
				v.add(c);
				for (int i = 0; i < this.a.length; i++)
					if (this.a[c][i] == 1 && !v.contains(i) && !q.contains(i))
						q.add(i);
			}
		} while (q.size() > 0);
		return false;
	}

	int calcLCA(int p, int q) {
		if (p == q)
			return p;
		if (findPath(p, q))
			return p;
		if (findPath(q, p))
			return q;
		int result = -1;
		int[] parr = new int[e];
		int[] qarr = new int[e];
		boolean[] pm = new boolean[v];
		boolean[] qm = new boolean[v];
		int pc = 0;
		int qc = 0;
		parr[pc] = 0;
		qarr[qc] = 0;
		for (int i = 0; i < v; i++) {
			pm[i] = false;
			qm[i] = false;
		}
		for (int i = 0; i < v; i++) {
			for (int j = 0; j < v; j++) {
				pm[p] = true;
				qm[q] = true;
				for (int k = 0; k < v; k++) {
					if (a[k][j] == 1 && pm[j]) {
						pc++;
						parr[pc] = k;
						pm[k] = true;
					}
					if (a[k][j] == 1 && qm[j]) {
						qc++;
						qarr[qc] = k;
						qm[k] = true;
					}
					if (qarr[qc] == parr[pc]) {
						i = v;
						j = v;
						k = v;
						result = qarr[qc];
					}
				}
			}
		}
		return result;
	}
}