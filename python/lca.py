class Node:
    def __init__(self,d):
        self.l = None
        self.r = None
        self.d = d

def findPath(node,p,d):
    if node is None:
        return False
    p.append(node.d)
    if node.d == d:
        return True
    if node.l != None and findPath(node.l,p,d):
        return True
    if node.r != None and findPath(node.r,p,d):
        return True
    p.pop()
    return False

def findLCA(root,x,y):
    p1 = []
    p2 = []
    if not findPath(root,p1,x):
        return -1
    if not findPath(root,p2,y):
        return -1
    i = 0
    while i < len(p1) and i < len(p2):
        if p1[i] != p2[i]:
            break
        i += 1
    return p1[i -1]