import unittest
from lca import Node
from lca import findPath
from lca import findLCA

class LCATest(unittest.TestCase):
    def test_empty(self):
        root = None
        self.assertEqual(findLCA(root,1,2),-1)
    def test_singleton(self):
        root = Node(1)
        self.assertEqual(findLCA(root,1,2),-1)
    def test_findPath(self):
        root = Node(1)
        root.l = Node(2)
        root.r = Node(3)
        root.r.r = Node(4)
        self.assertEqual(findPath(root,[],4),True)
        self.assertEqual(findPath(root,[],3),True)
        self.assertEqual(findPath(root,[],2),True)
    def test_lca(self):
        root = Node(1)
        root.l = Node(2)
        # Testing for correct answers
        self.assertEqual(findLCA(root,1,2),1)
        root.l.l = Node(3)
        self.assertEqual(findLCA(root,1,3),1)
        self.assertEqual(findLCA(root,2,3),2)
        root.l.r = Node(4)
        self.assertEqual(findLCA(root,4,3),2)
        root.r = Node(5)
        self.assertEqual(findLCA(root,5,3),1)
    def test_lca_error(self):
        root = Node(1)
        root.l = Node(2)
        root.r = Node(3)
        root.l.l = Node(4)
        root.l.r = Node(5)
        root.r.l = Node(6)
        root.r.r = Node(7)
        # Attempt to use fake nodes
        self.assertEqual(findLCA(root,4,100),-1)
        self.assertEqual(findLCA(root,500,2),-1)
        self.assertEqual(findLCA(root,500,600),-1)
        # Some final tests
        self.assertEqual(findLCA(root,7,5),1)
        self.assertEqual(findLCA(root,6,4),1)
        self.assertEqual(findLCA(root,6,7),3)

if __name__ == '__main__':
    unittest.main()